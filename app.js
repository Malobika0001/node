var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var config = require('./config');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var delhieatRouter = require('./routes/delhieat');
var delhistayRouter = require('./routes/delhistay');
var mumbaieatRouter =require ('./routes/mumbaieat');
var mumbaistayRouter = require('./routes/mumbaistay');

var bangaloreeatRouter = require('./routes/bangaloreeat');
var bangalorestayRouter = require('./routes/bangalorestay');
var goaeatRouter = require('./routes/goaeat');
var goastayRouter = require('./routes/goastay');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var passport = require('passport');
var authenticate = require('./authenticate');

const mongoose = require('mongoose');
const delhieat = require('./models/delhieat');
const uploadRouter = require('./routes/uploadRouter');

const url = config.mongoUrl;

const connect = mongoose.connect(url);

var app = express();
// Secure traffic only
app.all('*', (req, res, next) => {
  if (req.secure) {
    return next();
  }
  else {
    res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
  }
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/imageUpload',uploadRouter);




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');





app.use(passport.initialize());



app.use('/', indexRouter);
app.use('/users', usersRouter);
//autherization start






//autherization end



app.use('/delhieat', delhieatRouter);
app.use('/delhistay', delhistayRouter);
app.use('/mumbaieat', mumbaieatRouter);
app.use('/mumbaistay',mumbaistayRouter);
app.use ('/bangaloreeat', bangaloreeatRouter);
app.use('/bangalorestay', bangalorestayRouter);
app.use('/goaeat',goaeatRouter);
app.use('/goastay', goastayRouter)


connect.then((db) => {
  console.log("Connected correctly to server");
}, (err) => { console.log(err); });
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;


