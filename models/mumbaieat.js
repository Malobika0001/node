const mongoose = require('mongoose');
const Schema = mongoose.Schema;

require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const mumbaieatSchema =new Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    id:{
        type:String,
        required:true,
        unique:true,
    },
    description:{
        type:String,
        required:true,
        unique:false
    },
    location:{
        type:String,
        required:false,
        unique:false
    },
    image:{
        type:String,
        required:false,
        unique:false
    },
       
    });
var mumbaieat = mongoose.model('mumbaieat',mumbaieatSchema);
module.exports=mumbaieat;