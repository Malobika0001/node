const mongoose = require('mongoose');
const Schema = mongoose.Schema;

require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const goastaySchema =new Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    id:{
        type:String,
        required:true,
        unique:true,
    },
    description:{
        type:String,
        required:true,
        unique:false
    },
    hotel1:{
        type:String,
        required:false,
        unique:false
    },
    
    img1:{
        type:String,
        required:false,
        unique:false
        
    },
    price1:{
        type:String,
        required:false,
        unique:false

    },
    hotel2:{
        type:String,
        required:false,
        unique:false
    },
    
    img2:{
        type:String,
        required:false,
        unique:false
        
    },
    price2:{
        type:String,
        required:false,
        unique:false

    },
    hotel3:{
        type:String,
        required:false,
        unique:false
    },
    
    img3:{
        type:String,
        required:false,
        unique:false
        
    },
    price3:{
        type:String,
        required:false,
        unique:false

    },
    image:{
        type:String,
        required:false,
        unique:false
    },
        
    
    });
var goastay = mongoose.model('goastay',goastaySchema);
module.exports=goastay;