const mongoose = require('mongoose');
const Schema = mongoose.Schema;

require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const goaeatSchema =new Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    id:{
        type:String,
        required:true,
        unique:true,
    },
    description:{
        type:String,
        required:true,
        unique:false
    },
    location:{
        type:String,
        required:false,
        unique:false
    },
    image:{
        type:String,
        required:false,
        unique:false
    },
       
    });
var goaeat = mongoose.model('goaeat',goaeatSchema);
module.exports=goaeat;