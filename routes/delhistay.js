const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const delhistay = require('../models/delhistay');
const delhistayRouter = express.Router();

delhistayRouter.use(bodyParser.json());

delhistayRouter.route('/')
.get((req,res,next) => {
    delhistay.find({})
    .then((delhistays)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(delhistays);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    delhistay.create(req.body)
    .then((delhistays) => {
        console.log('Dish Created ', delhistays);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(delhistays);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    delhistay.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = delhistayRouter;