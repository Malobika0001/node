const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const bangalorestay = require('../models/bangalorestay');
const bangalorestayRouter = express.Router();

bangalorestayRouter.use(bodyParser.json());

bangalorestayRouter.route('/')
.get((req,res,next) => {
    bangalorestay.find({})
    .then((bangalorestays)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(bangalorestays);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    bangalorestay.create(req.body)
    .then((bangalorestays) => {
        console.log('Dish Created ', bangalorestays);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(bangalorestays);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    bangalorestay.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = bangalorestayRouter;