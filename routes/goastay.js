const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const goastay = require('../models/goastay');
const goastayRouter = express.Router();

goastayRouter.use(bodyParser.json());

goastayRouter.route('/')
.get((req,res,next) => {
    goastay.find({})
    .then((goastays)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(goastays);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    goastay.create(req.body)
    .then((goastays) => {
        console.log('Dish Created ', goastays);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(goastays);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    goastay.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = goastayRouter;