const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const mumbaistay= require('../models/mumbaistay');
const mumbaistayRouter = express.Router();

mumbaistayRouter.use(bodyParser.json());

mumbaistayRouter.route('/')
.get((req,res,next) => {
    mumbaistay.find({})
    .then((mumbaistays)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(mumbaistays);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    mumbaistay.create(req.body)
    .then((mumbaistays) => {
        console.log('Dish Created ', mumbaistays);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(mumbaistays);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    mumbaistay.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = mumbaistayRouter;