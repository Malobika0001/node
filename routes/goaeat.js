const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const goaeat = require('../models/goaeat');
const goaeatRouter = express.Router();

goaeatRouter.use(bodyParser.json());

goaeatRouter.route('/')
.get((req,res,next) => {
    goaeat.find({})
    .then((goaeats)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(goaeats);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    goaeat.create(req.body)
    .then((goaeats) => {
        console.log('Dish Created ', goaeats);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(goaeats);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    goaeat.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = goaeatRouter;