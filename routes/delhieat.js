const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var authenticate = require('../authenticate');

const delhieat = require('../models/delhieat');
const delhieatRouter = express.Router();

delhieatRouter.use(bodyParser.json());
const cors = require('./cors');

delhieatRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req,res,next) => {
    delhieat.find({})
    .then((delhieats)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(delhieats);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser,  (req, res, next) => {
    delhieat.create(req.body)
    .then((delhieats) => {
        console.log('Dish Created ', delhieats);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(delhieats);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,  (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    delhieat.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = delhieatRouter;