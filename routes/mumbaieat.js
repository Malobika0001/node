const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const mumbaieat = require('../models/mumbaieat');
const mumbaieatRouter = express.Router();

mumbaieatRouter.use(bodyParser.json());

mumbaieatRouter.route('/')
.get((req,res,next) => {
    mumbaieat.find({})
    .then((mumbaieats)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(mumbaieats);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    mumbaieat.create(req.body)
    .then((mumbaieats) => {
        console.log('Dish Created ', mumbaieats);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(mumbaieats);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    mumbaieat.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = mumbaieatRouter;