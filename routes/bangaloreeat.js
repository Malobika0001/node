const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');


const bangaloreeat = require('../models/bangaloreeat');
const bangaloreeatRouter = express.Router();

bangaloreeatRouter.use(bodyParser.json());

bangaloreeatRouter.route('/')
.get((req,res,next) => {
    bangaloreeat.find({})
    .then((bangaloreeats)=>{
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(bangaloreeats);
    },(err) => next(err))
    .catch((err) => next(err));
})
.post((req, res, next) => {
    bangaloreeat.create(req.body)
    .then((bangaloreeats) => {
        console.log('Dish Created ', bangaloreeats);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(bangaloreeats);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /rest');
})
.delete((req, res, next) => {
    bangaloreeat.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = bangaloreeatRouter;